/**
 * 
 */
package objetos;

/**
 * @author root
 *
 */
public class Empleado {
	//Atributos
	
	private String nombre;
	private String apellido;
	private int edad;
	private double salario;
	
	//Metodos
	
	/**
	 * Suma un plus al salario si el empleado tiene mas de 40 a�os
	 * @param sueldoPlus
	 **/
	public boolean plus(double sueldoPlus){
		boolean aumento=false;
		if(edad>30){
			salario+=sueldoPlus;
			aumento=true;
		}
		return aumento;
	}
	
	//Constructores
	/**
	 * Constructor por defecto
	 */
	public Empleado(){
		this.nombre="";
		this.apellido="";
		this.edad=0;
		this.salario=0;
	}
	/**
	public Empleado(){
		this("","",0,0);
	}
	*/
	 
	/**
	 * COnstructor con 4 parametros
	 * @param nombre: nombre del empleado
	 * @param apellido: apellido del empleado
	 * @param edad: edad del empleado
	 * @param salario: salario del empleado
	 */
	public Empleado(String nombre,String apellido,int edad,double salario){
		this.nombre=nombre;
		this.apellido=apellido;
		this.edad=edad;
		this.salario=salario;
	}
	
}
