/**
 * 
 */
package herencia01;

/**
 * @author root
 *
 */
public class electrodomestico {
	private double precio_base;
	private String color;
	private char consumo_energetico;
	private int peso;
	
	electrodomestico(){
		this.precio_base=100;
		this.color="blanco";
		this.consumo_energetico='F';
		this.peso=5;
	}
	electrodomestico(int peso,double precio){
		this.precio_base=precio;
		this.color="blanco";
		this.consumo_energetico='F';
		this.peso=peso;
	}
	electrodomestico(int peso,double precio,String color,char consumo){
		this.precio_base=precio;
		this.peso=peso;
		if(this.comprobarColor(color)){
			this.color=color;
		}
		else{
			this.color="blanco";
		}
		if(this.comprobarConsumoEnergetico(consumo)){
			this.consumo_energetico=consumo;
		}
		else{
			this.consumo_energetico=consumo;
		}
		
	}
	public double getPrecio_base() {
		return precio_base;
	}
	public String getColor() {
		return color;
	}
	public char getConsumo_energetico() {
		return consumo_energetico;
	}
	public double getPeso() {
		return peso;
	}
	
	public void setPrecio_base(double precio_base) {
		this.precio_base = precio_base;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public void setConsumo_energetico(char consumo_energetico) {
		this.consumo_energetico = consumo_energetico;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	private boolean comprobarConsumoEnergetico(char letra){
		boolean condicion=false;
		char[] array ={'A','B','C','D','E','F'};
		for(int i=0;i<array.length;i++){
			if (letra==array[i]){
				condicion=true;
			}
		}
		return condicion;
	}
	public boolean comprobarColor(String color){
		boolean condicion=false;
		String[] array ={"blanco","negro","rojo","azul","gris"};
		for(int i=0;i<array.length;i++){
			if (color.toLowerCase()==array[i]){
				condicion=true;
			}
		}
		return condicion;
	}
	public void precioFinal(){
		switch(this.consumo_energetico){
		case 'A':
			this.setPrecio_base(this.getPrecio_base()+100);
			break;
		case 'B':
			this.setPrecio_base(this.getPrecio_base()+80);
			break;
		case 'C':
			this.setPrecio_base(this.getPrecio_base()+60);
			break;
		case 'D':
			this.setPrecio_base(this.getPrecio_base()+50);
			break;
		case 'E':
			this.setPrecio_base(this.getPrecio_base()+30);
			break;
		case 'F':
			this.setPrecio_base(this.getPrecio_base()+10);
			break;
		}
		
		if (this.peso>=0 && this.peso<20){
			this.setPrecio_base(this.getPrecio_base()+10);
		}
		else if(this.peso>20 && this.peso<50){
			this.setPrecio_base(this.getPrecio_base()+50);	
		}
		else if(this.peso>=50 && this.peso<80){
			this.setPrecio_base(this.getPrecio_base()+80);	
		}
		else if(this.peso>80){
			this.setPrecio_base(this.getPrecio_base()+100);	
		}	
	}
}
