/**
 * 
 */
package herencia01;

/**
 * @author root
 *
 */
public class lavadora extends electrodomestico {
	public static final double CARGA_DEF=5;
	private double carga;
	
	lavadora(){
		super();
		this.carga=CARGA_DEF;
	}
	lavadora(int peso,double precio){
		super(peso,precio);
		this.carga=CARGA_DEF;
	}
	lavadora(int peso,double precio,String color,char consumo,double carga){
		super(peso,precio,color,consumo);
		this.carga=carga;
	}
	public double getCarga() {
		return carga;
	}
	public void precioFinal(){
		if (carga>30){
			this.setPrecio_base(this.getPrecio_base()+50);
		}
	}
}
