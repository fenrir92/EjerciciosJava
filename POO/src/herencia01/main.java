/**
 * 
 */
package herencia01;
import herencia01.electrodomestico;
/**
 * @author root
 *
 */
public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int suma=0;
		electrodomestico[] arrayelec= new electrodomestico[10];
		arrayelec[0]=new lavadora(50,100,"blanco",'B',10);
		arrayelec[1]=new lavadora(50,100,"blanco",'B',40);
		arrayelec[2]=new tv(100,50,"blanco",'B',21,true);
		arrayelec[3]=new tv(100,50,"blanco",'B',60,false);
		arrayelec[4]=new tv(100,50,"blanco",'B',60,true);
		arrayelec[5]=new tv(100,50,"blanco",'B',40,false);
		arrayelec[6]=new tv(100,50,"blanco",'B',40,true);
		arrayelec[7]=new electrodomestico(10,100,"blanco",'B');
		arrayelec[8]=new electrodomestico(50,100,"blanco",'A');
		arrayelec[9]=new electrodomestico(120,100,"blanco",'F');
		
		for(int i=0;i<arrayelec.length;i++){
			arrayelec[i].precioFinal();
		}
		for(int i=0;i<arrayelec.length;i++){
			System.out.println(arrayelec[i].getPrecio_base());
			suma+=arrayelec[i].getPrecio_base();
		}
		System.out.println("El precio de todos los electrodomesticos juntos es: "+suma);
	}

}
