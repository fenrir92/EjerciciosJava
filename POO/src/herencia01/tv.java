/**
 * 
 */
package herencia01;

/**
 * @author root
 *
 */
public class tv extends electrodomestico {
	private double resolucion;
	private boolean tdt;
	
	tv(){
		super();
		this.resolucion=20;
		this.tdt=false;
	}
	tv(double precio,int peso){
		super(peso,precio);
		this.resolucion=20;
		this.tdt=false;
	}
	tv(double precio,int peso,String color,char consumo,double resolucion, boolean tdt){
		super(peso,precio,color,consumo);
		this.resolucion=resolucion;
		this.tdt=tdt;
	}
	public double getResolucion() {
		return resolucion;
	}
	public void setResolucion(double resolucion) {
		this.resolucion = resolucion;
	}
	public boolean isTdt() {
		return tdt;
	}
	public void setTdt(boolean tdt) {
		this.tdt = tdt;
	}
	public void precioFinal(){
		if(resolucion>40){
			this.setPrecio_base(this.getPrecio_base()+(this.getPrecio_base()*.5));	
		}
		if(tdt){
			this.setPrecio_base(this.getPrecio_base()+50);
		}
	}
}
