package ej01;

public class persona {
	private static final char SEXO_DEFECTO ='H';
	//Atributos

	private String nombre;
	private int edad;
	private String DNI;
	private char sexo;
	private int peso;
	private int altura;
	
	//Constructores
	
	public persona(){
		this("",0,"",SEXO_DEFECTO,0,0);
	}
	public persona(String nombre, int edad, String DNI, char sexo, int peso, int altura) {
		this.nombre=nombre;
		this.edad=edad;
		this.DNI=DNI;
		this.sexo=sexo;
		this.peso=peso;
		this.altura=altura;
	}
	public persona(String nombre,int edad,char sexo){
		this.nombre=nombre;
		this.edad=edad;
		this.sexo=sexo;
	}
}
