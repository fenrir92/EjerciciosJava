/**
 * 
 */
package ej03;

/**
 * @author root
 *
 */
public class Electrodomestico {
	
	final static String colorDEF="blanco";
	final static String consumoDEF="F";
	final static int precioDEF=100;
	final static int pesoDEF=5;
	
	protected double precio_base;
	protected String consumo_energetico;
	protected int peso;
	protected String color;
	
	Electrodomestico(){
		this.precio_base=precioDEF;
		this.color=colorDEF;	
		this.consumo_energetico=consumoDEF;
		this.peso=pesoDEF;
	}
	Electrodomestico(int precio,int peso){
		this.precio_base=precio;
		this.consumo_energetico=consumoDEF;
		this.peso=peso;
		this.color=colorDEF;
	}
	Electrodomestico(int precio,int peso,String consumo,String color){
		this.precio_base=precio;
		this.consumo_energetico=consumo;
		this.peso=peso;
		this.color=color;
	}
	
	public boolean comprobarColor(String color){
		String colores[]={"Blanco","negro","rojo","azul","gris"};
		boolean condicion=false;
		for(int i=0;i<colores.length;i++){
			if(colores[i].equals(color)){
				condicion=true;
			}
		}
		return condicion;
	}
	
	
	
	public double getPrecio_base() {
		return precio_base;
	}
	public void setPrecio_base(double precio_base) {
		this.precio_base = precio_base;
	}
	public String getConsumo_energetico() {
		return consumo_energetico;
	}
	public void setConsumo_energetico(String consumo_energetico) {
		this.consumo_energetico = consumo_energetico;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public boolean comprobarConsumo(String electrodomesticos){
	String letra[]={"A","B","C","D","E","F"};
	boolean condicionLetra=false;
	for (int i=0;i<letra.length;i++){
		if(letra[i].equals(electrodomesticos)){
			condicionLetra=true;
		}
	}
	return condicionLetra;
	}
}
