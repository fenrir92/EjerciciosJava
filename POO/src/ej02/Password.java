/**
 * 
 */
package ej02;

/**
 * @author root
 *
 
2) Haz una clase llamada Password que siga las siguientes condiciones:

� Que tenga los atributos longitud y contrase�a . Por defecto, la longitud sera de 8.

� Los constructores ser�n los siguiente:

	o Un constructor por defecto.

	� Un constructor con la longitud que nosotros le pasemos. Generara una contrase�a

aleatoria con esa longitud.
*/
public class Password {
	private int longitud;
	private String contrase�a;
	
	public Password(){
		this.longitud=8;
		this.contrase�a="";
	}
	
	public Password(int longitud){
		this.longitud=longitud;
		this.contrase�a="";
		String letra[]={"A","B","C","D","E","F","G","H","I","J","K","L",
				"M","N","�","O","P","Q","R","S","T","U","V","W","X",
				"Y","Z","_",".",";",",",":","/","/","*","-","#","@","&",
				"1","2","3","4","5","6","7","8","9","0"};
		for(int i=0;i<longitud;i++){
			if((int)Math.floor(Math.random()*(9-1)+1)%2==0){
				this.contrase�a+=letra[((int)Math.floor(Math.random()*(letra.length-1)+1))];
			}
			else{
				this.contrase�a+=letra[((int)Math.floor(Math.random()*(letra.length-1)+1))].toLowerCase();

			}
			}
		
	}

	public String getContrase�a() {
		return contrase�a;
	}

	public void setContrase�a(String contrase�a) {
		this.contrase�a = contrase�a;
	}
	
}

