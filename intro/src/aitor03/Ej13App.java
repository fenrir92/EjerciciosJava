/**
 * 
 */
package aitor03;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej13App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Introduce el primer operando");
		int an = Integer.parseInt(a);
		String b=JOptionPane.showInputDialog("Introduce el segundo operando");
		int bn = Integer.parseInt(b);
		String signo=JOptionPane.showInputDialog("Introduce la operacion deseada:(+,-,*,/,^,%)");
		
		switch (signo){
		case "+":
			JOptionPane.showMessageDialog(null, "El resultado es: "+(an+bn));
			break;
		case "-":
			JOptionPane.showMessageDialog(null, "El resultado es: "+(an-bn));
			break;
		case "*":
			JOptionPane.showMessageDialog(null, "El resultado es: "+(an*bn));
			break;
		case "/":
			JOptionPane.showMessageDialog(null, "El resultado es: "+(double)(an*100/bn)/100);
			break;
		case "^":
			JOptionPane.showMessageDialog(null, "El resultado es: "+Math.pow(an,bn));
			break;
		case "%":
			JOptionPane.showMessageDialog(null, "El resultado es: "+(an%bn));
			break;
		default:
			JOptionPane.showMessageDialog(null, "Has introducido una operacion no valida");
		}
	}

}
