/**
 * 
 */
package aitor03;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej05App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String numero=JOptionPane.showInputDialog("Introduce un numero:");
		int num =Integer.parseInt(numero);
		if (num%2==0){
			JOptionPane.showMessageDialog(null, "El numero "+num+" es divisible entre 2");
		}
		else{
			JOptionPane.showMessageDialog(null, "El numero "+num+" no es divisible entre 2");
		}
	}

}
