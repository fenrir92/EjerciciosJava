/**
 * 
 */
package aitor04;

import java.util.Random;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej09App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Cuantos valores tendra el array?:");
		int an = Integer.parseInt(a);
		int[]array=new int[an];
		rellenaArray(0,9,array);
		muestraArray(array);
		totalArray(array);
	}
	public static void rellenaArray(int a, int b,int[]array){
		for(int i=0;i<array.length;i++){
			array[i]=aleatorio(a,b);
		}
	}
	public static void muestraArray(int[]array){
		for(int i=0;i<array.length;i++){
			System.out.println(array[i]);
		}
	}
	public static void totalArray(int[]array){
		int suma = 0;
		for(int i=0;i<array.length;i++){
			suma+=array[i];
		}
		System.out.println("La suma de los valores del array es: "+suma);
	}
	private static int aleatorio(int a,int b){
		Random rnd = new Random();
		return (int)(rnd.nextDouble()*(b-a)+a);
	}
}
