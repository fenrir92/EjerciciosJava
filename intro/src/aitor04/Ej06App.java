/**
 * 
 */
package aitor04;
import javax.swing.JOptionPane;
/**
 * @author root
 *
 */
public class Ej06App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Introduce un numero entero positivo:");
		int an = Integer.parseInt(a);
		if(an>=0){
			JOptionPane.showMessageDialog(null, "El numero tiene : "+numCifras(an)+" cifras");
		}
		else{
			JOptionPane.showMessageDialog(null, "Numero no valido,tiene que ser entero positivo");

		}
	}
	public static int numCifras(int a){
		int c=1;
		while(a/10!=0){
			a/=10;
			c++;
		}
		return c;
	}

}
