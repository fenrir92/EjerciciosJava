/**
 * 
 */
package aitor04;

import java.util.Random;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej10App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Cuantos valores tendra el array?:");
		String c=JOptionPane.showInputDialog("Rango inicial del valor aleatorio:");
		String d=JOptionPane.showInputDialog("Rango final del valor aleatorio:");
		int an = Integer.parseInt(a);
		int cn = Integer.parseInt(c);
		int dn = Integer.parseInt(d);
		int[]array=new int[an];
		rellenaArray(cn,dn,array);
		masAlto(array);
		
	}
	public static void rellenaArray(int a, int b,int[]array){
		for(int i=0,c=0;i<array.length;){
			c=aleatorio(a,b);
			if(es_primo(c)){array[i]=c;i++;}
			
		}
	}
	public static boolean es_primo(int a){
		boolean condicion=true;
		int c=0;
		if(a%2==0 && a!=2){
			for(int i=1;i<((int)Math.round(Math.sqrt(a)));i++){
				if(a%i==0){c++;}
			}
			if(c>2){condicion=false;}
		}
		return condicion;	
		}
	public static int aleatorio(int a,int b){
		Random rnd = new Random();
		return (int)(rnd.nextDouble()*(b-a)+a);
	}
	public static void masAlto(int[]array){
		int alto=0;
		for(int i=0;i<array.length;i++){
			if(alto<array[i]){alto=array[i];}
		}
		System.out.println("El valor mas alto del array es: "+alto);
	}
}
