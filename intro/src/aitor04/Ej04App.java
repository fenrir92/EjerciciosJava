/**
 * 
 */
package aitor04;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej04App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Introduce un numero:");
		int an = Integer.parseInt(a);
		JOptionPane.showMessageDialog(null, "El factorial de "+a+" es: "+factorial(an));
	}
	public static int factorial(int a){
		int f=1;
		for(int i =0;i<a;i++){
			f=f*(a-i);
		}
		return f;
	}
}
