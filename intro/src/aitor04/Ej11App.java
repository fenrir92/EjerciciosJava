/**
 * 
 */
package aitor04;

import java.util.Random;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej11App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Cuantos valores tendra el array?:");
		String c=JOptionPane.showInputDialog("Rango inicial del valor aleatorio:");
		String d=JOptionPane.showInputDialog("Rango final del valor aleatorio:");
		int an = Integer.parseInt(a);
		int cn = Integer.parseInt(c);
		int dn = Integer.parseInt(d);
		int[]array=new int[an];
		rellenaArray(cn,dn,array);
		int[]array2 = array;
		array=new int[an];
		muestraArray(array);
		muestraArray(array2);
		muestraArray(multiplicaArray(array,array2));
		
	}
	public static void muestraArray(int[]array){
		for(int i=0;i<array.length;i++){
			System.out.println(array[i]);
		}
	}
	public static int[] multiplicaArray(int[] array,int[] array2){
		int[]multiplicacion=new int[array.length];
		for(int i=0;i<array.length;i++){
			multiplicacion[i]=array[i]*array2[i];
		}
		return multiplicacion;
	}
	public static void rellenaArray(int a, int b,int[]array){
		for(int i=0;i<array.length;i++){
			array[i]=aleatorio(a,b);
		}
	}
	public static int aleatorio(int a,int b){
		Random rnd = new Random();
		return (int)(rnd.nextDouble()*(b-a)+a);
	}
}
