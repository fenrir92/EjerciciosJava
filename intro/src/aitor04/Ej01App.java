/**
 * 
 */
package aitor04;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej01App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String figura=JOptionPane.showInputDialog("De que figura quieres calcular el area?(triangulo,cuadrado,circulo");
		switch(figura){
		case "triangulo":
			String base=JOptionPane.showInputDialog("Introduce la longitut de la base del triangulo:");
			String altura=JOptionPane.showInputDialog("Introduce la altur del triangulo:");
			double basen = Double.parseDouble(base);
			double alturan = Double.parseDouble(altura);
			JOptionPane.showMessageDialog(null, "El area del triangulo es: "+area_triangulo(basen,alturan));
			break;
		case "circulo":
			String radio=JOptionPane.showInputDialog("Introduce el radio del circulo:");
			double radion = Double.parseDouble(radio);
			JOptionPane.showMessageDialog(null, "El area del circulo es: "+area_circulo(radion));
			break;
		case "cuadrado":
			area_cuadrado();
			break;
		default:
			JOptionPane.showMessageDialog(null, "Esa figura no es valida");
		}
	}
	public static double area_circulo(double radio){
		double area;
		final double PI =3.14;
		area=Math.pow(radio, 2)*PI;
		return(area);
	}
	public static double area_triangulo(double base,double altura){
		double area;
		area=(base*altura)/2;
		return(area);
	}
	public static void area_cuadrado(){
		double area;
		String lado=JOptionPane.showInputDialog("Introduce la longitud del lado del cuadrado:");
		double ladon = Double.parseDouble(lado);
		area=Math.pow(ladon, 2);
		JOptionPane.showMessageDialog(null, "El area del cuadrado es: "+area+" metros cuadrdos.");

	}
}
