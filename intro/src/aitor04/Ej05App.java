/**
 * 
 */
package aitor04;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej05App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Introduce un numero:");
		int an = Integer.parseInt(a);
		JOptionPane.showMessageDialog(null, "El numero en binario es: "+aBinario(an));
	}
	public static String aBinario(int a){
		int vector[]=new int[20];
		int i=0;
		String binario="";
		if (a==0){ binario=binario+"0";}
		else if (a==1){ binario=binario+"1";}
		else{
			while(a/2!=0){
				vector[i]=a%2;
				i++;
				a=a/2;
			}
			vector[i]=1;
			for(int j=i;j>=0;j--){
				binario=binario+Integer.toString(vector[j]);
			}
		}
		return binario;
	}
}
