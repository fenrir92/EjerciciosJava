/**
 * 
 */
package aitor04;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej07App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String euros=JOptionPane.showInputDialog("Introduce la cantidad de euros a convertir:");
		int eurosn = Integer.parseInt(euros);
		String moneda=JOptionPane.showInputDialog("A que moneda quieres convertirlos?(dolares,yenes,libras):");
		cambioMoneda(eurosn,moneda);
	}
	public static void cambioMoneda(double euros,String moneda){
		switch(moneda){
		case "dolares":
			JOptionPane.showMessageDialog(null, 0.86*euros+" Dolares.");
			break;
		case "yenes":
			JOptionPane.showMessageDialog(null, 1.28611*euros+" Yenes.");
			break;
		case "libras":
			JOptionPane.showMessageDialog(null, 129.852*euros+" Libras.");
			break;
		default:
			JOptionPane.showMessageDialog(null, "Moneda no valida");
		}
	}
}
