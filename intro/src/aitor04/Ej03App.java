/**
 * 
 */
package aitor04;

import javax.swing.JOptionPane;

/**
 * @author root
 *
 */
public class Ej03App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a=JOptionPane.showInputDialog("Introduce un numero:");
		int an = Integer.parseInt(a);
		if (es_primo(an)){
			JOptionPane.showMessageDialog(null, "Es primo");
		}
		else{
			JOptionPane.showMessageDialog(null, "No es primo");
		}
	}
	public static boolean es_primo(int a){
		boolean condicion=true;
		int c=0;
		if(a%2==0 && a!=2){
			for(int i=1;i<((int)Math.round(Math.sqrt(a)));i++){
				if(a%i==0){c++;}
			}
			if(c>2){condicion=false;}
		}
		return condicion;	
		}
	}
	

